# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 22:25:36 2024

@author: Andrea
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from math import sqrt

# Cargar los datos desde el archivo datos.csv
data = pd.read_csv('datos.csv')


# Crear un DataFrame con los datos proporcionados
# Estadísticas descriptivas para horas dedicadas
horas_stats = data['horas_dedicadas'].describe()

# Eliminar filas con calificaciones faltantes
data = data.dropna(subset=['calificaciones_obtenida'])

# Estadísticas descriptivas para calificaciones obtenidas
calificaciones_stats = data['calificaciones_obtenida'].describe()

# Análisis de regresión lineal
X = data['horas_dedicadas'].values.reshape(-1, 1)
y = data['calificaciones_obtenida'].values

# Crear y entrenar el modelo de regresión lineal
modelo_regresion = LinearRegression()
modelo_regresion.fit(X, y)

# Predecir calificaciones basadas en horas dedicadas
n = sum(map(int, str(int(data['horas_dedicadas'].sum() % 50))))  # Suma de dígitos módulo 50
prediccion_calificacion = modelo_regresion.predict(np.array([[n]]))

# Estadísticas del modelo de regresión
rmse = sqrt(mean_squared_error(y, modelo_regresion.predict(X)))
r2 = r2_score(y, modelo_regresion.predict(X))

# Gráfico de regresión lineal
plt.scatter(X, y, color='blue', label='Datos Originales')
plt.plot(X, modelo_regresion.predict(X), color='red', linewidth=2, label='Regresión Lineal')
plt.xlabel('Horas Dedicadas')
plt.ylabel('Calificaciones')
plt.legend()
plt.show()

# Imprimir resultados
print("Estadísticas para Horas Dedicadas:")
print(horas_stats)
print("\nEstadísticas para Calificaciones Obtenidas:")
print(calificaciones_stats)
print("\nPredicción de Calificación para", n, "horas dedicadas:", prediccion_calificacion[0])
print("\nRMSE (Error Cuadrático Medio):", rmse)
print("Coeficiente de Determinación (R^2):", r2)
